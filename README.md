# <div align="center"><a href="https://yoshitharathnayake.w3spaces.com/Index.html"><b><i>WELCOME!, TO MY OFFICIAL WEB SITE</i></b></a></div>

# <div align="center"><img src="Images/Yoshitha Rathnayake 2.png" width="300px"></div>

# [![Contributors](https://img.shields.io/badge/Contributors-1-lawngreen.svg?style=flat-square)](#contributors-)

Available at :  <b><i>[https://yoshitharathnayake.w3spaces.com/Index.html](https://yoshitharathnayake.w3spaces.com/Index.html)</i></b>

#
# Run Locally

### Installing Ruby and Jekyll

- Install Ruby and Jekyll
1. [macOS](https://jekyllrb.com/docs/installation/macos/) 
1. [Ubuntu](https://jekyllrb.com/docs/installation/ubuntu/) 
1. [Windows](https://jekyllrb.com/docs/installation/windows/).
1. [Other Linux](https://jekyllrb.com/docs/installation/other-linux/).

- Clone your forked repository
    
    ```
    git clone https://github.com/Yoshitha-SACK/My-Official-Web-Site.git
    cd My-Official-Web-Site
    ```
    
- Install all the dependencies 
    ```
    npm install 
    ```
    ```
    bundle install
    ```
    
    
- Start the server with 
    ```
    npm start
    ```
    ```
    bundle exec jekyll serve
    ``` 

## Contributors ✨

Thanks goes to these wonderful people ([emoji key](https://allcontributors.org/docs/en/emoji-key)):

<!-- ALL-CONTRIBUTORS-LIST:START - Do not remove or modify this section -->
<!-- prettier-ignore-start -->
<!-- markdownlint-disable -->
<table>
  <tr>
    <td align="center"><a href="https://yoshitharathnayake.w3spaces.com/Index.html"><img src="Images/Yoshitha Rathnayake 2.png" width="100px;" alt="Yoshitha Rathnayake"/><br /><sub><b>Yoshitha Rathnayake</b></sub></a><br/><a href="https://github.com/acf-sack/sack-site/commits?author=Yoshitha-SACK" title="Code">💻</a></td>
  </tr>
</table>

<!-- markdownlint-restore -->
<!-- prettier-ignore-end -->

<!-- ALL-CONTRIBUTORS-LIST:END -->

This project follows the [All Contributors](https://github.com/all-contributors/all-contributors) specification. Contributions of any kind welcome!
